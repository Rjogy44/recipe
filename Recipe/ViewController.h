//
//  ViewController.h
//  Recipe
//
//  Created by Randy Jorgensen on 2/1/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView* myTableView;
    
    NSArray* array;
}

@end