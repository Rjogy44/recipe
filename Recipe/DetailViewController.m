//
//  MyViewController.m
//  Recipe
//
//  Created by Randy Jorgensen on 2/6/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//


#import "DetailViewController.h"

@interface DetailViewController()

@end


@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sv = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sv.backgroundColor = [UIColor greenColor];
    [self.view addSubview:sv];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(125, 100, 125, 125)];
    iv.image = [UIImage imageNamed:self.infoDictionary[@"Image"]];
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [sv addSubview:iv];
    
//    UILabel* ti = [[UILabel alloc]initWithFrame:CGRectZero];
//    ti.text = self.infoDictionary[@"Title"];
//    ti.textAlignment = NSTextAlignmentCenter;
//    [ti setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
//    ti.translatesAutoresizingMaskIntoConstraints = NO;
//    [sv addSubview:ti];
    
    UILabel* ig = [[UILabel alloc]initWithFrame:CGRectZero];
    ig.text = @"Ingredients & Directions";
    [ig setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    ig.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:ig];
    
    UITextView* label2 = [[UITextView alloc]initWithFrame:CGRectZero];
    label2.text = self.infoDictionary[@"Ingredients"];
    label2.contentMode = UIViewContentModeScaleAspectFit;
    label2.translatesAutoresizingMaskIntoConstraints = NO;
    [sv addSubview:label2];
    
    
//    UILabel* di = [[UILabel alloc]initWithFrame:CGRectZero];
//    di.text = @"Directions";
//    [di setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
//    di.translatesAutoresizingMaskIntoConstraints = NO;
//    [sv addSubview:di];
//    
//    
//    UITextView* label3 = [[UITextView alloc]initWithFrame:CGRectZero];
//    label3.text = self.infoDictionary[@"Directions"];
//    label3.contentMode = UIViewContentModeScaleAspectFit;
//    label3.translatesAutoresizingMaskIntoConstraints = NO;
//    [sv addSubview:label3];
//    
    
    
    
    
    
   
    
    NSDictionary* variableDictionary = NSDictionaryOfVariableBindings(sv,iv,ig,label2);
    
    NSDictionary* metrics = nil;
    
//    
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[ti]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[ig]-|" options:0 metrics:metrics views:variableDictionary]];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[di]-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label2]-|" options:0 metrics:metrics views:variableDictionary]];
    
//    
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[label3]-|" options:0 metrics:metrics views:variableDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[iv]-[ig]-[label2]-|" options:0 metrics:metrics views:variableDictionary]];
    
    
    
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end