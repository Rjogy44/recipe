//
//  ViewController.m
//  Recipe
//
//  Created by Randy Jorgensen on 2/1/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString* path = [[NSBundle mainBundle]pathForResource:@"PropertyList" ofType:@"plist"];
    array = [[NSArray alloc]initWithContentsOfFile:path];
    
    //    array = @[@"Test"];
    
    myTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [self.view addSubview:myTableView];
    
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"test" forKey:@"key"];
    [defaults synchronize];
    
    
    NSString* string = [defaults objectForKey:@"key"];
    NSLog(@"%@", string);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* dictionary = array[indexPath.row];
    
    NSString* stringToDisplay = dictionary[@"Title"];
    
    cell.textLabel.text = stringToDisplay;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary* dictionary = array[indexPath.row];
    
    DetailViewController* dvc = [DetailViewController new];
    dvc.infoDictionary = dictionary;
    [self.navigationController pushViewController:dvc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
